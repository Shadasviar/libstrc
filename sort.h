/*
 * This file is part of libstrc. 
 * sort.h
 * Copyright (C) Uladzislau Harbuz, Maksym Yakymyshyn, 2018
 * 
 * libstrc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libstrc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SORT_H
#define SORH_H

#define uint8_t unsigned char
#define size_t uint8_t

void swap (uint8_t *a, uint8_t *b);
void bubble_sort (uint8_t *array, size_t size);
void insert_sort (uint8_t *array, size_t size);

#endif /* SORT_H */
