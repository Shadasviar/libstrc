/*
 * This file is part of libstrc. 
 * main.c
 * Copyright (C) Uladzislau Harbuz, Maksym Yakymyshyn, 2018
 * 
 * libstrc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libstrc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Uladzislau Harbuz, Yakymyshyn Maksym
 * sdcc 3.6.0 Linux throw USB to RS232 converter
 */

#include "defines.h"
#include <8051.h>
#include "strc51_time.h"
#include "strc51_serial.h"
#include "sort.h"

#define MAX_ARRAY_SIZE 1024

void func_isr(void) __interrupt 3 {
    counter_timer();
}

static int size = 0;
__xdata static uint8_t array1[MAX_ARRAY_SIZE];
__xdata static uint8_t array2[MAX_ARRAY_SIZE];

static void read_array() {
    uint8_t i = 0;
    size = (uint8_t)getchar();
    getchar(); // ignore space
    if (size > MAX_ARRAY_SIZE) return;

    while (i < size) {
        array1[i] = (uint8_t)getchar();
        array2[i] = array1[i];
        getchar(); //ignore next space;
        ++i;
    }
}

static void print_array(uint8_t* a, uint8_t size) {
    uint8_t i = 0;
    putchar(size);
    putchar(' ');

    while (i < size) {
        putchar(a[i++]);
        putchar(' ');
        msleep(5);
    }
}

static void unsafe_put_str_ln(char* str) {
    while (*str != '\n') {
        putchar(*str++);
        msleep(5);
    }
}

main (void) {  
    while (1) {
        uint8_t i = 0;
        uint8_t x = 0;

        struct time tm = {0};
        struct time tmtmp = {0};
        struct time print_time = {0};
   
        init();
        init_time();

        read_array();
        gettime(&tm);
        bubble_sort(array1, size);
        gettime(&tmtmp);
        diff_time(&tmtmp, &tm, &print_time);

        unsafe_put_str_ln("bubble sort: \n");
        print_array(array1, size);
        unsafe_put_str_ln("time:\n");
        putchar(print_time.msecs);
        unsafe_put_str_ln(" ms.\n");

        sleep(5);
   
        gettime(&tm);
        insert_sort(array2, size);
        gettime(&tmtmp);
        diff_time(&tmtmp, &tm, &print_time);

        unsafe_put_str_ln("insertion sort: \n");
        print_array(array1, size);
        unsafe_put_str_ln("time:\n");
        putchar(print_time.msecs);
        unsafe_put_str_ln(" ms.\n");
    }
}
