/*
 * This file is part of libstrc. 
 * strc51_time.h
 * Copyright (C) Uladzislau Harbuz, Maksym Yakymyshyn, 2018
 * 
 * libstrc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libstrc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TIME_H
#define TIME_H

/* sleep in milliseconds. 
 * The MOST PRECISIOUS ARE SECONDS! 
 * This function has VERY LARGE time speed unprecision.
 * use it only for short times, 
 * if need more then some seconds use sleep.
 */
void msleep(int n);

/* sleep in seconds */
void sleep(int n);

void init_time();

/* put call of it in your counter interrupt
 * catcher function. 
 */
void counter_timer();

/* Get only milliseconds in the current second. */
int getmsecs();
long getsecs();

struct time {
    long secs;
    int msecs;
};

void gettime(struct time* out);

/* Return 1 if a > b, 0 if a == b, -1 if a < b */
int cmp_time(struct time *a, struct time *b);

/* Return a - b */
void diff_time(struct time *a, struct time *b, struct time *out);

#endif /* TIME_H */
