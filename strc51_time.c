/*
 * This file is part of libstrc. 
 * strc51_time.c
 * Copyright (C) Uladzislau Harbuz, Maksym Yakymyshyn, 2018
 * 
* libstrc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libstrc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "defines.h"
#include "strc51_time.h"
#include <8051.h>

static volatile long posixtime = 0;

/* Used only for msleep because it can store max 10 minuts.*/
static volatile int milliseconds = 0;
static volatile int mcounter = 0;

/* Hz */
static const long cnt_freq = 3640;


void msleep(int n) {
    mcounter = 0;
    while(mcounter < n) __asm__(nop);
}

/* sleep in seconds */
void sleep(int n) {
    long targettime = posixtime + n;
    while(posixtime < targettime) __asm__(nop);
}

void init_time() {
    TMOD &= 0x0F;
    TMOD |= 0x20; 
    IE = 0x88;
}

void counter_timer() {
    static int t = 0;
    static char mt = 0;
    
    if (++mt > 3) {
        mt = 0;
        ++milliseconds;
        ++mcounter;
    }
    
    if (++t >= cnt_freq) {
        ++posixtime;
        milliseconds = 0;
        t = 0;
    }
    
}

int getmsecs() {
    return milliseconds;
}

long getsecs() {
    return posixtime;
}

void gettime(struct time *out) {
    out->secs = posixtime;
    out->msecs = milliseconds;
}

/* Return 1 if a > b, 0 if a == b, -1 if a < b */
int cmp_time(struct time *a, struct time *b) {
    if (a->secs == b->secs) {
        if (a->msecs < b->msecs) return -1;
        if (a->msecs > b->msecs) return 1;
        return 0;
    }
    if (a->secs < b->secs) return -1;
    return 1;
}

void diff_time(struct time *a, struct time *b, struct time *out) {
    out->secs = a->secs - b->secs;
    out->msecs = a->msecs - b->msecs;
}
