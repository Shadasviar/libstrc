/*
 * This file is part of libstrc. 
 * sort.c
 * Copyright (C) Uladzislau Harbuz, Maksym Yakymyshyn, 2018
 * 
 * libstrc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libstrc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "sort.h"

void swap (uint8_t *a, uint8_t *b) {
	*b ^= (*a ^= *b);
	*a ^= *b;
}

void bubble_sort (uint8_t* array, size_t size) {
    uint8_t i = 0;
    uint8_t j = 0;
    
    for (i = 0; i < size-1; ++i) {
        for (j = 0; j < size -i -1; ++j) {
            if (array[j] > array[j+1]) {
                swap(array+j, array+j+1);
            }
        }
    }
}

void insert_sort (uint8_t *array, size_t size) {
	uint8_t* min = array;
	uint8_t i = 0;
	uint8_t j = 0;

	for (; i < size; ++i) {
		min = array + i;
		for (j = i; j < size; ++j) {
			if (array[j] < *min) min = array+j; 
		}
		swap(array+i, min);
	}
}
