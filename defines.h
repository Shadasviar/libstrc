/*
 * This file is part of libstrc. 
 * defines.h
 * Copyright (C) Uladzislau Harbuz, Maksym Yakymyshyn, 2018
 * 
 * libstrc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libstrc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DEF_H
#define DEF_H

#if __SDCC_VERSION_MAJOR <= 1

#define __interrupt interrupt
#define __sfr sfr
#define __at at
#define __bit bit
#define __asm__(x) _asm x _endasm;
#define __xdata xdata

#else 
#define __asm__(x) __asm__(#x)
#endif

#endif /* DEF_H */
