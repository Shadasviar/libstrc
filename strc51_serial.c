/*
 * This file is part of libstrc. 
 * strc51_serial.c
 * Copyright (C) Uladzislau Harbuz, Maksym Yakymyshyn, 2018
 * 
 * libstrc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libstrc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "defines.h"
#include "strc51_serial.h"
#include <8051.h>

void init()
{
    SCON = 0x50;
    TMOD &= 0x0F;
    TMOD |= 0x20;
    TH1 = TL1 = 253;
    TCON = 0x40;
    PCON = 0x80;
}

char getchar()
{
    while(!RI);
    RI = 0;
    return SBUF;
}

void putchar(char c)
{
    while(!TI);
    SBUF = c;
    TI=0;
}
