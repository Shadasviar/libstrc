/*
 * This file is part of libstrc. 
 * lcd.h
 * Copyright (C) Uladzislau Harbuz, Maksym Yakymyshyn, 2018
 * 
 * libstrc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libstrc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LCD_H
#define LCD_H

void LcdInit();
void LcdClrScr();
void ShowProgressTriangle(char idx);
void LcdPrintHex16(unsigned int x);

void set_cursor_pos(char x, char y);
void print_LCD(unsigned char data);

#endif /* LCD_H */
